#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <iostream>

#include <QLineEdit>
#include <QCompleter>
#include <QTextCursor>
#include <QAbstractItemView>
#include <QScrollBar>
#include <QTextLayout>
#include <QPalette>
struct AutoCompleteLE : public QLineEdit{

	AutoCompleteLE(const QStringList& completions,QWidget *parent = 0):QLineEdit(parent){
		completer = new QCompleter(completions,this);
		completer->setWidget(this);
		completer->setCompletionMode(QCompleter::PopupCompletion);

		auto defaultPalette=palette();
		defaultPalette.setColor(QPalette::Text,Qt::red);
		setPalette(defaultPalette);

		auto defaultFont=font();
		defaultFont.setItalic(true);
		defaultFont.setUnderline(true);
		setFont(defaultFont);

		setBackgroundRole(QPalette::ColorRole::Base);

		connect(this,&QLineEdit::selectionChanged,this,&AutoCompleteLE::popupAutocomplete);/*handling double ckick selection*/

		/*autocomplete replace action*/
		connect(completer, static_cast<void(QCompleter::*)(const QString &)>(&QCompleter::activated),this,
				  [this](QString chosenCompletion){
			auto info=currentWordInfo(text());

			setText(text().replace(info.first,info.second,chosenCompletion));
			setCursorPosition(info.first+chosenCompletion.size());
		});

		/*syntax highlighting*/{
			highlightformat.setFontItalic(false);
			highlightformat.setFontUnderline(false);
			highlightformat.setForeground(QBrush(QLineEdit().palette().color(QPalette::ColorRole::Text)));

			connect(this, &QLineEdit::textChanged,this,[this](const QString & newText){
				QList<QInputMethodEvent::Attribute> attributes;

				for (int i = 0; i < completer->model()->rowCount(); ++i) {
					auto word=completer->model()->index(i,0).data().toString();
					int searchFrom=0;
					for (;;) {
						auto index=newText.indexOf(word,searchFrom);
						if (index==-1) {
							break;
						}
						searchFrom=index+1;

						attributes.append(QInputMethodEvent::Attribute(
													QInputMethodEvent::TextFormat,
													index - cursorPosition(),
													word.size(),
													highlightformat
													)
												);
					}
				}

				QInputMethodEvent event(QString(), attributes);
				QLineEdit::event(&event);
			});
		}
	}

	void keyPressEvent(QKeyEvent *e)override{
		QLineEdit::keyPressEvent(e);
		popupAutocomplete();
	}

private:
	//! show a popup displaying the current completions
	void popupAutocomplete(){
		auto current=text();
		auto info=currentWordInfo(current);
		completer->setCompletionPrefix(current.mid(info.first,cursorPosition()-info.first));

		QRect cr = cursorRect();
		cr.setWidth(completer->popup()->sizeHintForColumn(0) + completer->popup()->verticalScrollBar()->sizeHint().width());
		completer->complete(cr);
	}

	//! {start index,width}
	//! if the user selected text we use it as is
	//! else we use the word under the cursor
	//! (a word is a string of char separeted by spaces for other words
	std::pair<int,int> currentWordInfo(const QString & text){
		if(selectionLength()){
			return std::make_pair(selectionStart(), selectionLength());
		}else{
			int startIndexOfWord=text.leftRef(cursorPosition()).lastIndexOf(" ") + 1;
			int end__IndexOfWord;{
				const auto& rightText=text.midRef(cursorPosition());
				if(rightText.indexOf(" ")==-1){
					end__IndexOfWord = cursorPosition() + rightText.size();
				}else{
					end__IndexOfWord = cursorPosition() + rightText.indexOf(" ");
				}
			}

			return std::make_pair(
						startIndexOfWord,
						end__IndexOfWord-startIndexOfWord
						);
		}
	};

	QCompleter* completer;
	QTextCharFormat highlightformat;
};


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	setCentralWidget(new AutoCompleteLE({"<",">","==","and","or","true","false","(",")"," "}));
}

MainWindow::~MainWindow(){
	delete ui;
}
